

/**
 * Enum for Pokemon Types
 * @TODO possibly put the Type chart in here
 * @readonly
 * @enum {string}
 */
export const Type = Object.freeze({
  NULL: 'null',
  NORML: 'normal',
  FIRE: 'fire',
  WATER: 'water',
  ELEC: 'electric',
  GRASS: 'grass',
  ICE: 'ice',
  FIGHT: 'fighting',
  POISN: 'poison',
  GRND: 'ground',
  FLYNG: 'flying',
  PSYCH: 'psychic',
  BUG: 'bug',
  ROCK: 'rock',
  GHOST: 'ghost',
  DRAGN: 'dragon',
  DARK: 'dark',
  STEEL: 'steel',
  FAIRY: 'fairy'
});
