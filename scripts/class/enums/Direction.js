/**
 * Enum for hex directions
 * @readonly
 * @enum {string}
 */
export const Direction = Object.freeze({
  E: 'E',
  NE: 'NE',
  NW: 'NW',
  SE: 'SE',
  SW: 'SW',
  W: 'W'
});
