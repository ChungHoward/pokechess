/* eslint-disable indent */
/**
 * Enum to represent the current game state
 * @readonly
 * @enum {string}
 */
export const GameState = Object.freeze({
  GAME_START: '',
    MATCHMAKING: '',
    BATTLE_START: '',
      TURN_START: '',
        MAIN_PHASE: '',
        BATTLE_PHASE: '',
          LOOP_UPDATE: '',
      TURN_END: '',
    BATTLE_END: '',
  GAME_END: ''
});
