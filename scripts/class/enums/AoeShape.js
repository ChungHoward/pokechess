/**
 * Enum for determining which tiles adjacent to the target are also affected
 * @readonly
 * @enum {number}
 */
export const AoeShape = Object.freeze({
  SINGLE: 1,  // Hits target only
  LONG2: 2,   // Hits target and 1 tile behind target    ↗ T
  WIDE: 3,    // Hits target and 2 tiles beside target: A → T
  LONG3: 3,   // Hits target and 2 tiles behind target   ↘ T                                      ↗ T
  CONE: 4,    // Hits target, the tile behind it, and the 2 tiles both targets are touching: A → T → T
  LONG4: 4,   // Hits target and 3 tiles behind it                                                ↘ T
  CIRCLE: 6,  // Hits all tiles around the target, excluding the target
  ALL7: 7,    // Hits all tiles around the target, including the target
  ALL19: 19,  // Hits all tiles with radius 2 around the target, including the target
});
