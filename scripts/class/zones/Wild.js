
import Deck from './Deck.js';
import Pokemon from '../pokemon/Pokemon.js';

export default class Wild {
  /**
   * @class
   */
  constructor() {
    this.deck = new Deck();
    this.currentLevel = 1;
    /** @type {Function[]} Array of functions that are called to further add/filter the library */
    this.encounterMods = [];
    this.encounterRate = [
      //   1,  2,  3,  4,  5 ← Pokemon Rarity
      [0,  0,  0,  0,  0,  0], // ↓ Trainer level
      [0, 90, 10,  0,  0,  0], // 1
      [0, 70, 30,  0,  0,  0], // 2
      [0, 55, 30, 15,  0,  0], // 3
      [0, 40, 30, 25,  5,  0], // 4
      [0, 29, 29, 31, 10,  1], // 5
      [0, 24, 28, 31, 15,  2], // 6
      [0, 20, 24, 31, 20,  5], // 7
      [0, 10, 19, 31, 30, 10]  // 8
    ];
    this.isLocked    = false;
    this.library     = this.deck.allPokemon;
    this.maxPokemon  = 5;
    this.refreshCost = 1;

    /** @type {Pokemon[]} */
    this.pokemon = [];
    while (this.pokemon.length < 6) {
      this.pokemon.push(this.library.pop());
    }
    /** @type {HTMLImageElement[]} */
    this.pokemonImgs = [];
    this.refreshWildPkmnImgs();
  }

  /**
   * Filters the library so that a proprotional amount of Pokemon with the given encounterRate appear.
   * Should be called whenever the trainer levels up or encounter rate is modified in any way.
   * @param {number} level The trainer's level
   */
  filterWildPokemon(level) {
    this.currentLevel = level;
    this.library = [];
    const encounterRateAtLevel = this.encounterRate[level];
    for (let i = 1; i < encounterRateAtLevel.length; i++) {
      if (encounterRateAtLevel[i] === 0) { return; }
      let pkmnAtRarity = this.deck.allPokemon.filter(pkmn => pkmn.rarity === i);
      pkmnAtRarity = this.shuffle(pkmnAtRarity);
      this.library.push(...pkmnAtRarity.slice(0, encounterRateAtLevel[i]));
    }
    // TODO Do something like Skill.handleExtraEffects using this.encounterMods on this.library
  }

  lock() {
    this.isLocked = true;
  }

  /**
   * Draw 6 new pokemon from the {@see Deck} and put them in wildPkmn.
   * If deck runs out of cards, shuffle a new Deck and draw until wildPkmn.length = 6
   */
  refresh() {
    this.pokemon = [];
    if (this.library.length <= 6) {
      this.pokemon.push(...this.library);
      this.filterWildPokemon(this.currentLevel);
    }
    this.shuffle();
    while (this.pokemon.length < 6) {
      this.pokemon.push(this.library.pop());
    }
    this.refreshWildPkmnImgs();
  }

  /**
   * Refresh the sprites in this.pokemonImgs using this.pokemon
   */
  refreshWildPkmnImgs() {
    this.pokemonImgs = [];
    this.pokemon.forEach((pkmn, i) => {
      this.pokemonImgs[i] = new Image();
      this.pokemonImgs[i].onerror = (event) => console.error(`Could not find ${this.trainer.wild.pokemonImgs[i].src}\n${event}`);
      this.pokemonImgs[i].src = pkmn.spriteUrl;
    });
  }

  /**
   * @template T
   * @param {Array.<T>} array Shuffles the library using Durstenfeld shuffle, but can be used to shuffle any array
   * @returns {Array.<T>} the shuffled array
   */
  shuffle(array = this.library) {
    for (let i = array.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [array[i], array[j]] = [array[j], array[i]];
    }
    return array;
  }

  unlock() {
    this.isLocked = false;
  }

}
