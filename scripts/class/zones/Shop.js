import { AllItems } from '../../lib/Item.lib.js';
import Item from '../items/Item.js';
import Trainer from '../trainer/Trainer.js';

export default class Shop {
  /**
   * @class
   */
  constructor() {
    this.isLocked    = false;
    this.library     = this.shuffle(AllItems.slice(0));
    this.refreshCost = 1;

    /** @type {Item[]} TODO Will need to filter library for items the Trainer is elligible to buy */
    this.items = [];
    while (this.items.length < 6) {
      this.items.push(this.library.pop());
    }
    /** @type {HTMLImageElement[]} Holds the scaled image elements in the shop */
    this.shopItemImgs = [];
    this.populateShopItemImgs();
  }

  lock() {
    this.isLocked = true;
  }

  /**
   * Refresh the sprites in the shop window
   */
  populateShopItemImgs() {
    this.shopItemImgs = [];
    this.items.forEach((item, i) => {
      this.shopItemImgs[i] = new Image();
      this.shopItemImgs[i].src = item.spriteUrl;
    });
  }

  /**
   * Draw 6 items from the {@see Deck} and put them in items.
   * If deck runs out of cards, shuffle a new Deck and draw until items.length = 6
   */
  refresh() {
    this.items = [];
    if (this.library.length <= 6) {
      this.items.push(...this.library);
      this.library = this.shuffle(AllItems.slice(0));
    }
    while (this.items.length < 6) {
      this.items.push(this.library.pop());
    }
  }

  /**
   * @template T
   * @param {Array.<T>} array Shuffles library using Durstenfeld shuffle, but can be used to shuffle any array
   * @returns {Array.<T>} the shuffled array
   */
  shuffle(array = this.library) {
    for (let i = array.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [array[i], array[j]] = [array[j], array[i]];
    }
    return array;
  }

  unlock() {
    this.isLocked = false;
  }

}
