import Item from '../items/Item.js';
import { PokeBall } from '../../lib/Item.lib.js';

export default class Inventory {

  constructor() {
    /** @type {Item[]} */
    this.items = [];
    this.items.push(PokeBall);

    /** @type {HTMLImageElement[]} Holds the scaled image elements in the shop */
    this.itemImgs = [];

    const img = new Image();
    img.src = PokeBall.spriteUrl;
    this.itemImgs.push(img);
  }

  /**
   * @param {Item} item Counts the number of this specified item
   * @returns {number} The number of the specified item in Inventory
   */
  amountOwned(item) {
    return this.items.reduce((count, thisItem) => thisItem === item ? count + 1 : count, 0);
  }

  /**
   * @param {Item} item The item to remove from inventory
   */
  consumeItem(item) {
    const index = this.items.findIndex(theItem => theItem === item);
    this.items.splice(index, 1);
    this.itemImgs.splice(index, 1);
  }

}
