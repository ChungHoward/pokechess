import Pokemon from '../pokemon/Pokemon.js';

export default class Team {

  constructor() {
    /** @type {Pokemon[]} */
    this.pokemon = [];
  }

  /**
   * @returns {boolean} true if all Pokemon on this team has fainted
   */
  isDefeated() {
    return this.pokemon.every(pkmn => pkmn.currentHP <= 0);
  }

}
