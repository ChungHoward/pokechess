
import Bench from './Bench.js';
import Inventory from './Inventory.js';
import { PokeBall } from '../../lib/Item.lib.js';
import Pokeball from '../items/Pokeball.js';
import Pokemon from '../pokemon/Pokemon.js';
import Shop from '../zones/Shop.js';
import Team from './Team.js';
import Wild from '../zones/Wild.js';

export default class Trainer {

  /**
   * @class
   * @param {string} name
   */
  constructor(name = 'Player') {
    /** @type {Trainer[]} */
    this.badges = [];
    this.name = name;
    this.currency = 100;
    this.exp = 0;
    this.level = 1;
    this.wins = 0;
    this.loses = 0;
    this.bench = new Bench();
    this.inventory = new Inventory();
    this.team = new Team();
    this.shop = new Shop();
    this.wild = new Wild();
    /**
     * Experience to Next Level. If you are at index 1, you are level 1.
     */
    this._expTNL = [-1, 0, 4, 7, 11, 18, 29, 47, 76];
    this.filterWildPokemon();
    this.wild.refresh();

    // TODO: Set the trainer color from benchPanel instead of here
    this.color = '#000000';
    if (name === 'left') this.color = '#880000';
    if (name === 'right') this.color = '#008800';
  }

  /**
   * Spend 3 currency to gain 3 exp.
   */
  buy3Exp() {
    if (this.currency < 3) { return; }
    this.currency -= 3;
    this.exp += 3;
    this.checkLevelUp();
  }

  /**
   * @param {number} index Purchases the item at index from the shop
   */
  buyItemAtIndex(index) {
    if (this.currency < index.price || index < 0) { return; }
    const item = this.shop.items[index];
    this.currency -= item.price;
    this.shop.items.splice(index, 1);
    this.inventory.items.push(item);
    // Add sprite to inventory
    const img = new Image();
    img.src = item.spriteUrl;
    this.inventory.itemImgs.push(img);
  }

  /**
   * Purchase a shop refresh.
   */
  buyShopRefresh() {
    if (this.currency < this.shop.refreshCost) { return; }
    this.currency -= this.shop.refreshCost;
    this.shop.refresh();
  }

  /**
   * Purchase a wild refresh.
   */
  buyWildRefresh() {
    if (this.currency < this.wild.refreshCost) { return; }
    this.currency -= this.wild.refreshCost;
    this.wild.refresh();
  }

  /**
   * Trainers can catch any rarity=1 Pokemon for 1 currency, but higher rarity Pokemon will require special Pokeballs
   * @param {number} index Catches the pokemon at index from the wild
   */
  catchPokemonAtIndex(index) {
    if (index < 0) { return; }
    const pokemon = this.wild.pokemon[index];
    // Trainers always have access to normal PokeBalls, so use it when possible
    if (PokeBall.canCatch(pokemon)) {
      this.currency--;
      this.bench.pokemon.push(pokemon);
      this.bench.pokemonImgs.push(this.wild.pokemonImgs[index]);
      this.wild.pokemon.splice(index, 1);
      return;
    }
    const usablePokeballs = this.getUsablePokeballs(pokemon);
    if (usablePokeballs.length === 1) {
      this.inventory.consumeItem(usablePokeballs[0]);
      this.bench.pokemon.push(pokemon);
      this.bench.pokemonImgs.push(this.wild.pokemonImgs[index]);
      this.wild.pokemon.splice(index, 1);
    }
    // TODO What should happen if you own multiple pokeballs that can catch that pokemon?
  }

  /**
   * Checks to see if current exp is enough to level up. Should be called every time exp is gained.
   */
  checkLevelUp() {
    const newLevel = this._expTNL.indexOf(this._expTNL.find(exp => exp >= this.exp));
    if (this.level >= newLevel) { return; }
    // Level Up!
    this.level = newLevel;
    this.filterWildPokemon();
  }

  /**
   * @param {Pokemon} pokemon The pokemon to catch
   * @returns {Pokeball} PokeBall if it is usable, otherwise an owned pokeball that could catch the given pokemon
   */
  choosePokeball(pokemon) {
    const usablePokeballs = this.getUsablePokeballs(pokemon);
    const indexOfPokeBall = usablePokeballs.indexOf(PokeBall);
    if (indexOfPokeBall >= 0) { return PokeBall; }
    else if (usablePokeballs.length === 1) { return usablePokeballs[0]; }
  }

  /**
   * Filters the Wild Pokemon library according to this trainer's level
   */
  filterWildPokemon() {
    this.wild.filterWildPokemon(this.level);
  }

  /**
   * @param {number} exp The amount of exp to gain
   */
  gainExp(exp) {
    this.exp += exp;
    this.checkLevelUp();
  }

  /**
   * @returns {number} The amount of exp needed to level up
   */
  getExpTNL() {
    return this._expTNL[this.level + 1] - this.exp;
  }

  /**
   * @param {Pokemon} pokemon The Pokemon to attempt to catch
   * @returns {Pokeball[]} An array of Pokeballs that could catch the given pokemon
   */
  getUsablePokeballs(pokemon) {
    /** @type {Pokeball[]} */
    const ownedPokeballs = this.inventory.items.filter(item => item.getClassName === PokeBall.getClassName);
    return ownedPokeballs.filter(ball => ball.canCatch(pokemon));
  }

}
