import Item from './Item.js';
import Pokemon from '../pokemon/Pokemon.js';

export default class Pokeball extends Item {
  /**
   * @callback callbackfn
   * @param {Pokemon} pokemon Checks if the given pokemon is catchable
   * @returns {boolean}
   */
  /**
   * @param {string} name The name of the item
   * @param {number} price The amount of currency the item costs in the {@link Shop}
   * @param {number} rarity The level requirement of the player for this item to spawn in the {@link Shop}
   * @param {string} desc The description of the item. Try not to exceed 64 characters.
   * @param {callbackfn} canCatch returns true if the ball can catch the given pokemon, false otherwise
   */
  constructor(name, price = 1, rarity = 1, desc, canCatch = (pokemon) => false) {
    super(name, price, rarity, desc, name //? `assets/sprites/items/${name.toLowerCase().replace(' ', '-')}.png` : '');
      ? `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/items/${name.toLowerCase().replace(' ', '-')}.png` : '');
    this.canCatch = canCatch;
  }

}
