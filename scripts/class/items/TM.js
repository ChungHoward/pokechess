import Item from './Item.js';
import Pokemon from '../pokemon/Pokemon.js';
import Skill from '../pokemon/Skill.js';

export default class TM extends Item {
  /**
   * @param {string} name The name of the item
   * @param {number} price The amount of currency the item costs in the {@link Shop}
   * @param {number} rarity The level requirement of the player for this item to spawn in the {@link Shop}
   * @param {string} desc The description of the item. Try not to exceed 64 characters.
   * @param {Skill}  skill The skill to teach a {@link Pokemon} when this item is used
   */
  constructor(name, price = 1, rarity = 1, desc, skill) {
    super(name, price, rarity, desc, name //? `assets/sprites/items/${name.toLowerCase().replace(' ', '-')}.png` : '');
      ? `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/items/tm-${skill.type}.png` : '');
    this.skill = skill;
  }

  /**
   * @param {Pokemon} pokemon Teaches the pokemon the skill in this TM
   */
  teachSkill(pokemon) {
    if (pokemon.types.includes(this.skill.type))
      pokemon.skill = this.skill;
  }

}
