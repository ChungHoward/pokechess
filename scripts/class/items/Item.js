
export default class Item {

  /**
   * @interface Item
   * @param {string} name 
   * @param {number} price 
   * @param {number} rarity 
   */
  constructor(name = '', price = 0, rarity = 0, desc = '', spriteUrl = '') {
    this.name   = name;
    this.price  = price;
    this.rarity = rarity;
    this.desc   = desc;
    this.spriteUrl = spriteUrl;
  }

  getClassName() {
    return this.constructor.name;
  }

}
