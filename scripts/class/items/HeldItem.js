import Item from './Item.js';
import Pokemon from '../pokemon/Pokemon.js';

export default class HeldItem extends Item {
  /**
   * @callback triggerfn
   * @param {Pokemon} pokemon
   * @returns {boolean} returns true when this Held Item should activate
   * @callback effectfn
   * @param {Pokemon} pokemon
   * @returns {void}
   */
  /**
   * @param {string} name The name of the item
   * @param {number} price The amount of currency the item costs in the {@link Shop}
   * @param {number} rarity The level requirement of the player for this item to spawn in the {@link Shop}
   * @param {string} desc The description of the item. Try not to exceed 64 characters.
   * @param {triggerfn} trigger returns true when this Held Item should activate
   * @param {effectfn} effect The action to perform on the {@link Pokemon} when trigger returns true
   */
  constructor(name = '', price = 1, rarity = 1, desc = '', trigger = (pokemon) => false, effect = (pokemon) => { }) {
    super(name, price, rarity, desc, name //? `assets/sprites/items/${name.toLowerCase().replace(' ', '-')}.png` : '');
      ? `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/items/${name.toLowerCase().replace(' ', '-')}.png` : '');
    this.trigger = trigger;
    this.effect = effect;
  }

}
