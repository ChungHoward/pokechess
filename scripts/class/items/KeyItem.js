import Item from './Item.js';

export default class KeyItem extends Item {

  /**
   * @param {string} name The name of the item
   * @param {number} price The amount of currency the item costs in the {@link Shop}
   * @param {number} rarity The level requirement of the player for this item to spawn in the {@link Shop}
   * @param {string} desc The description of the item. Try not to exceed 64 characters.
   */
  constructor(name, price = 1, rarity = 1, desc) {
    super(name, price, rarity, desc, name //? `assets/sprites/items/${name.toLowerCase().replace(' ', '-')}.png` : '');
      ? `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/items/${name.toLowerCase().replace(' ', '-')}.png` : '');
  }

}
