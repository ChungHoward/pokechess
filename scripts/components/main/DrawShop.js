
import { Font } from '../../class/enums/Font.js';
import ICanvas from '../ICanvas.js';
import Point from '../Point.js';
import Rect from '../Rect.js';
import Trainer from '../../class/trainer/Trainer.js';

export default class DrawShop extends ICanvas {

  /**
   * @class
   * @param {Trainer} trainer The current player
   */
  constructor(trainer) {
    super();
    this.trainer              = trainer;
    this.shopItemCount        = 6;           // Can be greater than 6 to make room in the shop for more items/elements/components
    this.clickedShopItemIndex = -1;          // The index of the most recently clicked Shop Item
    this.clickedShopItemRect  = new Rect();  // The Rect that the clicked item sprite lives in
    this.imgRect              = new Rect();  // The Rect that shop item sprites live in
    this.tooltipRect          = new Rect();  // The Rect that the tooltip text lives in

    this.canvas.addEventListener('mousedown', event => this.onClick(event));
    this.trainer.shop.populateShopItemImgs();

    /** @type {Rect[]} */
    this.shopItemRects = [];
    for (let i = 0; i < this.shopItemCount; i++) {
      this.shopItemRects.push(new Rect());
    }
    /** This is the rect of the shop zone measured as a percentage of the screen */
    this.zoneRect = new Rect(0, 0, 0.2, 0.75);
  }

  /**
   * Attaches the sprite of the clicked item to your cursor. It goes away when you click something else.
   */
  drawClickedShopItem() {
    if (this.clickedShopItemIndex < 0) { return; }
    // Draw item sprite
    this.clickedShopItemRect.setAll(
      this.lastMousePos.x - this.trainer.shop.shopItemImgs[this.clickedShopItemIndex].width * 1.2,
      this.lastMousePos.y - this.trainer.shop.shopItemImgs[this.clickedShopItemIndex].height * 1.2,
      this.trainer.shop.shopItemImgs[this.clickedShopItemIndex].width  * this.scaleRatio(),
      this.trainer.shop.shopItemImgs[this.clickedShopItemIndex].height * this.scaleRatio());
    this.context.drawImage(this.trainer.shop.shopItemImgs[this.clickedShopItemIndex], ...this.clickedShopItemRect);
    const mouseIsOverShopItemIndex = this.lastMousePos.indexOfBoundingRect(...this.shopItemRects);
    if (mouseIsOverShopItemIndex >= 0) { return; }
    // If cursor is not over a shop item and is holding a shop item, draw additional tooltip
    const tooltipText = `💰 -${this.trainer.shop.items[this.clickedShopItemIndex].price}`;
    const textWidth = this.context.measureText(tooltipText).width * 1;
    const textHeight = Font.SIZE_MD * this.scaleRatio() * 2;
    // Draw tooltip rect
    this.tooltipRect.setAll(
      this.lastMousePos.x - textWidth * 0.5,
      this.lastMousePos.y - textHeight * 2,
      textWidth,
      textHeight);
    this.context.fillStyle = 'rgba(0, 0, 0, 0.7)';
    this.context.fillRect(...this.tooltipRect);
    // Draw tooltip text
    this.context.fillStyle = 'rgba(255, 255, 255, 1)';
    this.context.font = `${Font.SIZE_MD * this.scaleRatio()}px ${Font.STYLE}`;
    this.context.textAlign = 'center'
    this.context.fillText(tooltipText,
      this.lastMousePos.x,
      this.lastMousePos.y - this.shopItemRects[0].h * 0.5);
  }

  /**
   * Draws all of the items and their contents inside the shop
   */
  drawShopItems() {
    // truncate shopItemRects if player purchases items
    if (this.shopItemRects.length > this.trainer.shop.items.length) {
      this.shopItemRects.length = this.trainer.shop.items.length;
    }
    // append more Rects if player refreshes shop
    while (this.shopItemRects.length < this.trainer.shop.items.length) {
      this.shopItemRects.push(new Rect());
    }
    for (let i = 0; i < this.trainer.shop.items.length; i++) {
      this.context.clearRect(...this.shopItemRects[i]);
      // Draw shop item rect
      this.shopItemRects[i].setAll(
        this.rect.w * 0.03,                      // padding-left = 0.03
        this.rect.h / this.shopItemCount * i,    // height = 1/6 of shopHeight
        this.rect.w * 0.94,                      // margin-right = 0.03, so width = 0.94
        this.rect.h / this.shopItemCount * 0.9); // padding-bottom = 0.1, so height = 0.9 since theres no padding-top
      this.context.fillStyle = 'rgba(0, 100, 0, 0.3)';
      this.context.fillRect(...this.shopItemRects[i]);
      // Draw sprites
      this.imgRect.setAll(
        this.rect.w * 0.06, // margin-left = 0.03 (+0.03 from shopItemRect)
        this.rect.h / this.shopItemCount * i + this.rect.h * (0.1 / this.shopItemCount), // I have no idea why this works so well
        this.trainer.shop.shopItemImgs[i].width  * this.scaleRatio(),
        this.trainer.shop.shopItemImgs[i].height * this.scaleRatio());
      this.context.fillRect(...this.imgRect);
      this.context.drawImage(this.trainer.shop.shopItemImgs[i], ...this.imgRect);
      // Draw item name
      this.context.font = `${Font.SIZE_LG * this.scaleRatio()}px ${Font.STYLE}`;
      this.context.fillStyle = 'rgba(0, 0, 0, 1)';
      this.context.textAlign = 'left';
      this.context.fillText(this.trainer.shop.items[i].name,
        this.shopItemRects[i].x + this.imgRect.w * 1.25,
        this.shopItemRects[i].y + this.shopItemRects[i].h * 0.3);
      // Draw rarity
      this.context.font = `${Font.SIZE_SM * this.scaleRatio()}px ${Font.STYLE}`;
      this.context.fillText('⭐'.repeat(this.trainer.shop.items[i].rarity),
        this.shopItemRects[i].x + this.imgRect.w * 1.25,
        this.shopItemRects[i].y + this.shopItemRects[i].h * 0.58);
      // Draw price
      this.context.fillText(`💰 ${this.trainer.shop.items[i].price}`,
        this.shopItemRects[i].x + this.imgRect.w * 1.25,
        this.shopItemRects[i].y + this.shopItemRects[i].h * 0.85);
      // Draw class name (TODO convert to an icon or perhaps colored fill/border)
      this.context.fillText(this.trainer.shop.items[i].getClassName(),
        this.shopItemRects[i].x + this.imgRect.w * 3,
        this.shopItemRects[i].y + this.shopItemRects[i].h * 0.58);
    }
    // Draw empty shop item rect in place of purchased items
    for (let i = this.shopItemCount - 1; i >= this.trainer.shop.items.length; i--) {
      const placeholderRect = new Rect(
        this.rect.w * 0.03,
        this.rect.h / this.shopItemCount * i,
        this.rect.w * 0.94,
        this.rect.h / this.shopItemCount * 0.9);
      this.context.fillStyle = 'rgba(0, 100, 0, 0.2)';
      this.context.fillRect(...placeholderRect);
    }
  }

  /**
   * Draws a tooltip after mouseover on a shopItemRect for secondsToDrawTooltip seconds
   */
  drawTooltip() {
    if (this.framesSinceCursorMoved < this.secondsToDrawTooltip * this.frameRate) {
      // TODO use this time to animate tooltip growing from 0% to 100%
      this.framesSinceCursorMoved++;
    } else {
      const mouseIsOverShopItemIndex = this.lastMousePos.indexOfBoundingRect(...this.shopItemRects);
      // if mouse is over shop item and not holding a shop item, draw Tooltip
      if (mouseIsOverShopItemIndex >= 0 && this.clickedShopItemIndex < 0
        && mouseIsOverShopItemIndex < this.trainer.shop.items.length) {
        const desc        = this.trainer.shop.items[mouseIsOverShopItemIndex].desc; // Shop item description text
        const descLine1   = desc.slice(0, desc.indexOf(' ', desc.length / 2));      // Custom word wrap, first line
        const descLine2   = desc.slice(desc.indexOf(' ', desc.length / 2) + 1);     // Second line of wrapped description
        this.context.font = `${Font.SIZE_SM * this.scaleRatio()}px ${Font.STYLE}`;  // Font size affects textWidth
        const textWidth   = Math.max(this.context.measureText(descLine1).width, this.context.measureText(descLine2).width) + 30;
        const textHeight  = Font.SIZE_SM * 3.3 * this.scaleRatio();
        // Draw tooltip rect
        this.tooltipRect.setAll(...this.lastMousePos, textWidth, textHeight);
        this.context.fillStyle = 'rgba(0, 0, 0, 0.7)';
        this.context.fillRect(...this.tooltipRect);
        // Draw item description
        this.context.textAlign = 'left';
        this.context.fillStyle = 'rgba(255, 255, 255, 1)';
        this.context.fillText(descLine1,
          this.lastMousePos.x + 15,
          this.lastMousePos.y + this.shopItemRects[0].h / 5);
        this.context.fillText(descLine2,
          this.lastMousePos.x + 15,
          this.lastMousePos.y + this.shopItemRects[0].h / 2.5);
      }
    }
  }

  /**
   * @param {MouseEvent} event From Event Listener
   */
  onClick(event) {
    const canvasRect          = this.canvas.getBoundingClientRect();
    const clickPos            = new Point(event.clientX - canvasRect.left, event.clientY - canvasRect.top);
    const indexOfItemOnCursor = this.clickedShopItemIndex;
    this.clickedShopItemIndex = clickPos.indexOfBoundingRect(...this.shopItemRects);
    // if an empty shop item rect is clicked, do nothing
    if (this.clickedShopItemIndex >= this.trainer.shop.items.length) {
      this.clickedShopItemIndex = -1;
      return;
    }
    // if holding an item and did not click on a shop item again, purchase the item
    if (indexOfItemOnCursor >= 0 && this.clickedShopItemIndex < 0) {
      this.trainer.buyItemAtIndex(indexOfItemOnCursor);
      // Refresh the sprites in the shop zone
      this.trainer.shop.populateShopItemImgs();
    } else if (indexOfItemOnCursor >= 0) {
      this.clickedShopItemIndex = -1;
    }
  }

  update() {
    this.context.clearRect(...this.rect);
    this.rect.setAll(
      this.zoneRect.x * this.canvas.width,
      this.zoneRect.y * this.canvas.height,
      this.zoneRect.w * this.canvas.width,
      this.zoneRect.h * this.canvas.height);
    this.context.fillStyle = 'rgba(0, 100, 0, 0.2)';
    this.context.fillRect(...this.rect);

    this.drawShopItems();
    // this.drawClickedShopItem(); // this is now called in MainPhase.js
    this.drawTooltip();
    this.updateFPS(); // inherited from super class
    // Placeholder Text
    this.context.font = `30px ${Font.STYLE}`;
    this.context.fillStyle = 'rgba(0, 0, 0, 0.5)';
    this.context.textAlign = 'center';
    this.context.fillText('Shop', this.rect.x + this.rect.w / 2, this.rect.y + this.rect.h / 2);
  }

}
