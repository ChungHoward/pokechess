
import { Font } from '../../class/enums/Font.js';
import ICanvas from '../ICanvas.js';
import Rect from '../Rect.js';
import Trainer from '../../class/trainer/Trainer.js';

export default class DrawInventory extends ICanvas {

  /**
   * @class
   * @param {Trainer} trainer The current player
   */
  constructor(trainer) {
    super();
    this.trainer     = trainer;
    this.imgRect     = new Rect();  // The Rect that inventory item sprites live in
    this.invItemCol  = 5;           // The number of columns in the inventory
    this.invItemRow  = 3;           // The number of rows in the inventory
    this.tooltipRect = new Rect();  // The Rect that the tooltip text lives in
    /** @type {Rect[][]} */
    this.invRects = [];
    for (let row = 0; row < this.invItemRow; row++) {
      this.invRects[row] = [];
      for (let col = 0; col < this.invItemCol; col++) {
        this.invRects[row][col] = new Rect();
      }
    }
    /** This is the rect of the inventory zone measured as a percentage of the screen */
    this.zoneRect = new Rect(0.75, 0.7, 0.25, 0.3);
  }

  /**
   * Draws a table containing the items in your inventory
   */
  drawInventoryItems() {
    this.invRects.forEach(col => col.forEach(row => this.context.clearRect(...row)));
    // Draw inventory item table
    for (let row = 0; row < this.invItemRow; row++) {
      for (let col = 0; col < this.invItemCol; col++) {
        this.invRects[row][col].setAll(
          // 1st set of parens is the top/left padding. 2nd set of parens is space between each rect.
          this.rect.x + (this.rect.w * 0.03) + (this.rect.w * col / this.invItemCol * 0.96),
          this.rect.y + (this.rect.h * 0.04) + (this.rect.h * row / this.invItemRow * 0.98),
          this.rect.h / this.invItemCol * 1.4,   // hidth of table cell
          this.rect.h / this.invItemCol * 1.4);  // height of table cell
        this.context.fillStyle = 'rgba(0, 100, 100, 0.3)';
        this.context.fillRect(...this.invRects[row][col]);
        // Draw inventory item sprites
        const i = row * this.invItemCol + col;
        if (this.trainer.inventory.itemImgs[i]) {
          const itemImgW = this.trainer.inventory.itemImgs[i].width * this.scaleRatio() * 1;
          const itemImgH = this.trainer.inventory.itemImgs[i].height * this.scaleRatio() * 1;
          this.context.drawImage(this.trainer.inventory.itemImgs[i],
            this.invRects[row][col].x + (this.invRects[row][col].w * 0.5) - (itemImgW * 0.5),
            this.invRects[row][col].y + (this.invRects[row][col].h * 0.5) - (itemImgH * 0.5),
            itemImgW, itemImgH);
        }
      }
    }
  }

  /**
   * Draws a tooltip after mouseover on an inventoryRect for secondsToDrawTooltip seconds
   */
  drawTooltip() {
    if (this.framesSinceCursorMoved < this.secondsToDrawTooltip * this.frameRate) {
      // TODO use this time to animate tooltip growing from 0% to 100%
      this.framesSinceCursorMoved++;
    } else {
      /** @type {Rect[]} Flatten to 1d array */
      const invRectArr = this.invRects.flat();
      const mouseIsOverInventoryIndex = this.lastMousePos.indexOfBoundingRect(...invRectArr);
      // if mouse is over shop item and not holding a shop item, draw Tooltip
      if (mouseIsOverInventoryIndex >= 0 && mouseIsOverInventoryIndex < this.trainer.inventory.items.length) {
        const name        = this.trainer.inventory.items[mouseIsOverInventoryIndex].name;
        const desc        = this.trainer.inventory.items[mouseIsOverInventoryIndex].desc;
        const descLine1   = desc.slice(0, desc.indexOf(' ', desc.length / 2));  // Custom word wrap, first line
        const descLine2   = desc.slice(desc.indexOf(' ', desc.length / 2) + 1); // Second line of wrapped description
        this.context.font = `${Font.SIZE_SM * this.scaleRatio()}px ${Font.STYLE}`;
        // Draw tooltip rect
        const textWidth  = Math.max(this.context.measureText(descLine1).width, this.context.measureText(descLine2).width) + 20;
        const textHeight = Font.SIZE_SM * 5 *  this.scaleRatio();
        this.tooltipRect.setAll(
          this.lastMousePos.x - textWidth,
          this.lastMousePos.y - textHeight,
          textWidth, textHeight);
        this.context.fillStyle = 'rgba(0, 0, 0, 0.7)';
        this.context.fillRect(...this.tooltipRect);
        // Draw item name and description in tooltip
        this.context.textAlign = 'left';
        this.context.fillStyle = 'rgba(255, 255, 255, 1)';
        this.context.font = `${Font.SIZE_MD * this.scaleRatio()}px ${Font.STYLE}`;
        this.context.fillText(name,
          this.lastMousePos.x - textWidth + 10,
          this.lastMousePos.y - textHeight * 0.7);
        this.context.font = `${Font.SIZE_SM * this.scaleRatio()}px ${Font.STYLE}`;
        this.context.fillText(descLine1,
          this.lastMousePos.x - textWidth + 10,
          this.lastMousePos.y - textHeight * 0.4);
        this.context.fillText(descLine2,
          this.lastMousePos.x - textWidth + 10,
          this.lastMousePos.y - textHeight * 0.15);
      }
    }
  }

  update() {
    this.context.clearRect(...this.rect);
    this.rect.setAll(
      this.zoneRect.x * this.canvas.width,
      this.zoneRect.y * this.canvas.height,
      this.zoneRect.w * this.canvas.width,
      this.zoneRect.h * this.canvas.height);
    this.context.fillStyle = 'rgba(0, 100, 100, 0.2)';
    this.context.fillRect(...this.rect);

    this.drawInventoryItems();
    this.drawTooltip();
    this.updateFPS(); // inherited from super class

    this.context.font = `30px ${Font.STYLE}`;
    this.context.fillStyle = 'rgba(0, 0, 0, 0.5)';
    this.context.textAlign = 'center';
    this.context.fillText('Inventory', this.rect.x + this.rect.w / 2, this.rect.y + this.rect.h / 2);
  }

}
