
import { Font } from '../../class/enums/Font.js';
import Hex from '../Hex.js';
import ICanvas from '../ICanvas.js';
import Rect from '../Rect.js';
import Trainer from '../../class/trainer/Trainer.js';

export default class DrawTeam extends ICanvas {

  /**
   * @class
   * @param {Trainer} trainer The current player
   */
  constructor(trainer) {
    super();
    this.trainer = trainer;
    this.teamCol = 8;
    this.teamRow = 3;

    /** @type {Hex[][]} Contains each of the hexagons that team pokemon sprites live in */
    this.teamHexagons = [];
    for (let row = 0; row < this.teamCol; row++) {
      this.teamHexagons[row] = [];
      for (let col = 0; col < this.teamCol; col++) {
        this.teamHexagons[row][col] = new Hex();
      }
    }
    /** This is the rect of the team zone measured as a percentage of the screen */
    this.zoneRect = new Rect(0.2, 0.2, 0.55, 0.5);
  }

  /**
   * TODO Currently the color is wrong. Not sure why
   */
  drawTeamHex() {
    this.context.fillStyle = 'rgba(255, 0, 0, 0.08)';
    for (let col = 0; col < 8; col++) {
      for (let row = 0; row < 3; row++) {
        if (row % 2 === 1 && col === 7) { continue; }
        this.teamHexagons[row][col].setAll(
          this.rect.x + this.rect.w * 0.07 + (col + (row % 2 === 1 ? 0.5 : 0)) * this.rect.w * 0.123,
          this.rect.y + this.rect.y * 0.88 + (row * this.rect.h * 0.24),
          this.rect.w * 0.065);
        this.teamHexagons[row][col].fill(this.context);
      }
    }
  }

  update() {
    this.context.clearRect(...this.rect);
    this.rect.setAll(
      this.zoneRect.x * this.canvas.width,
      this.zoneRect.y * this.canvas.height,
      this.zoneRect.w * this.canvas.width,
      this.zoneRect.h * this.canvas.height);
    this.context.fillStyle = 'rgba(100, 0, 0, 0.2)';
    this.context.fillRect(...this.rect);

    this.drawTeamHex();

    this.context.font = `30px ${Font.STYLE}`;
    this.context.fillStyle = 'rgba(0, 0, 0, 0.5)';
    this.context.textAlign = 'center';
    this.context.fillText('Team', this.rect.x + this.rect.w / 2, this.rect.y + this.rect.h / 2);
  }

}
