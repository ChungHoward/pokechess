
import DrawAvatar from './main/DrawAvatar.js';
import DrawBench from './main/DrawBench.js';
import DrawInventory from './main/DrawInventory.js';
import DrawPreview from './main/DrawPreview.js';
import DrawShop from './main/DrawShop.js';
import DrawTeam from './main/DrawTeam.js';
import DrawWild from './main/DrawWild.js';
import ICanvas from './ICanvas.js';
import Trainer from '../class/trainer/Trainer.js';

export default class MainPhase extends ICanvas {
  /**
   * @class
   */
  constructor() {
    super();
    this.trainer = new Trainer('Player 1');

    this.iCanvas = new ICanvas();
    this.drawAvatar = new DrawAvatar(this.trainer);
    this.drawBench = new DrawBench(this.trainer);
    this.drawInventory = new DrawInventory(this.trainer);
    this.drawPreview = new DrawPreview(this.trainer);
    this.drawShop = new DrawShop(this.trainer);
    this.drawTeam = new DrawTeam(this.trainer);
    this.drawWild = new DrawWild(this.trainer);
  }

  toggleCursor() {
    if (this.iCanvas.lastMousePos.indexOfBoundingRect(
      this.drawAvatar.buyExpRect,
      this.drawAvatar.refreshShopRect,
      ...this.drawBench.benchRects.flat().slice(0, this.trainer.bench.pokemon.length),
      ...this.drawInventory.invRects.flat().slice(0, this.trainer.inventory.items.length),
      ...this.drawShop.shopItemRects,
      this.drawWild.refreshWildRect,
      ...this.drawWild.wildPkmnRects
    ) >= 0) {
      document.body.style.cursor = 'pointer';
    } else {
      document.body.style.cursor = 'auto';
    }
  }

  /**
   * Gets called every frame to render UI. The order update() is called matters.
   * It determines the z-axis of each element.
   */
  update() {
    this.iCanvas.update();
    this.drawTeam.update();
    this.drawBench.update();
    this.drawAvatar.update();
    this.drawPreview.update();
    this.drawInventory.update();
    this.drawWild.update();
    this.drawShop.update();
    // Cursors are always on top so they are drawn last
    this.toggleCursor();
    this.drawBench.drawClickedBenchPkmn();
    this.drawShop.drawClickedShopItem();
    this.drawWild.drawClickedWildPkmn();
    requestAnimationFrame(() => this.update());
  }

}
