import Rect from './Rect.js';

export default class Point {

  /**
   * Represents a point in 2-d space. Used to cursor position and handle MouseEvents
   * @param {number} x left
   * @param {number} y top
   */
  constructor(x = 0, y = 0) {
    this.x = x;
    this.y = y;
  }

  /**
   * @param {number} x left
   * @param {number} y top
   */
  setAll(x, y) {
    this.x = x;
    this.y = y;
  }

  /**
   * @param  {...Rect} rects The Rect(s) to check contact with
   * @returns the first index of the given rects if this point is touching the rect
   */
  indexOfBoundingRect(...rects) {
    return rects.findIndex(rect =>
      this.x >= rect.x
      && this.x <= rect.x + rect.w
      && this.y >= rect.y
      && this.y <= rect.y + rect.h);
  }

  /**
   * Makes instances of Point iterable. Enables use of the spread operator, for-of loops, etc.
   * @example
   * const point = new Point(2, 4);
   * logPoint = (a, b) => console.log(`${a} ${b}`);
   * logPoint(point.x, point.y); // 2 4
   * logPoint(...point); // 2 4
   */
  *[Symbol.iterator]() {
    yield this.x;
    yield this.y;
  }

}
