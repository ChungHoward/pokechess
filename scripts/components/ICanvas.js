
import { Font } from '../class/enums/Font.js';
import Point from './Point.js';
import Rect from './Rect.js';

export default class ICanvas {

  /**
   * @interface ICanvas
   */
  constructor() {
    this.aspectRatio   = 18 / 9; // widescreen is 16/9, so this is a tiny bit wider
    /** @type {HTMLCanvasElement} */
    this.canvas        = document.getElementById('iCanvas');
    /** @type {CanvasRenderingContext2D} */
    this.context       = this.canvas.getContext('2d');
    this.canvas.height = window.innerHeight;
    this.canvas.width  = window.innerWidth;
    this.rect          = new Rect(0, 0, this.canvas.width, this.canvas.height);
    /** Counter to calculate frame rate */
    this.frameCount   = 0;
    this.frameRate    = 0;
    this.elapsedTime  = performance.now();
    this.lastMousePos = new Point();
    /** Used to delay showing Tooltips. Will later animate */
    this.framesSinceCursorMoved = 0;
    this.secondsToDrawTooltip   = 0.2;
    // Track the mouse position so we can handle tooltips without knowing the cursor position from a MouseEvent
    this.canvas.addEventListener('mousemove', event => this.getMousePos(event));
  }

  /**
   * @param {MouseEvent} event 
   * @returns {Point} The Point your mouse is located at
   */
  getMousePos(event) {
    const canvasRect = this.canvas.getBoundingClientRect();
    this.framesSinceCursorMoved = 0;
    this.lastMousePos.setAll(event.clientX - canvasRect.left, event.clientY - canvasRect.top);
  }

  /**
   * @returns {number} Adjusts the size of elements to the current canvas size
   */
  scaleRatio() {
    return this.canvas.width / 666;
  }

  setCanvasSizeUsingAspectRatio() {
    if (window.innerWidth / window.innerHeight < this.aspectRatio) {
      this.canvas.width = window.innerWidth;
      this.canvas.height = window.innerWidth / this.aspectRatio;
    } else if (window.innerWidth / window.innerHeight > this.aspectRatio) {
      this.canvas.height = window.innerHeight;
      this.canvas.width = window.innerHeight * this.aspectRatio;
    }
  }

  /**
   * @param {string} str
   * @returns {string} Capitalizes the first letter of each word in str
   */
  titleCase(str) {
    return str.split(' ').map(word => word.charAt(0).toUpperCase() + word.slice(1)).join(' ');
  }

  update() {
    this.context.clearRect(...this.rect);
    this.setCanvasSizeUsingAspectRatio();
    this.rect.setAll(0, 0, this.canvas.width, this.canvas.height);
    /* Background color for canvas not needed right now */
    // this.context.fillStyle = 'rgba(0, 0, 0, 0.1)';
    // this.context.fillRect(...this.rect);

    this.context.font = `30px ${Font.STYLE}`;
    this.context.fillStyle = 'rgba(0, 0, 0, 0.5)';
    this.context.textAlign = 'center';
    this.context.fillText('Background', this.rect.x + this.rect.w / 2, this.rect.y + this.rect.h / 2);

    this.updateFPS();

    this.context.font = `14px ${Font.STYLE}`;
    this.context.fillStyle = 'rgba(0, 0, 0, 1)';
    this.context.textAlign = 'right';
    this.context.fillText(`${this.frameRate} FPS`, this.rect.x + this.rect.w, this.rect.y + 14);
  }

  updateFPS() {
    this.frameCount++;
    if (performance.now() - this.elapsedTime >= 1000) {
      this.frameRate = this.frameCount;
      this.frameCount = 0;
      this.elapsedTime = performance.now();
    }
  }

}
