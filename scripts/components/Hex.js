import Point from './Point.js';

export default class Hex {

  /**
   * Bounding box for canvas components and elements
   * @param {number} x left
   * @param {number} y top
   * @param {number} r radius
   */
  constructor(x = 0, y = 0, r = 0) {
    this.x = x;
    this.y = y;
    this.r = r;
  }

  /**
   * @param {Point} point The point to check
   * @returns {boolean} Returns true if the point is within this Rect
   */
  containsPoint(point) {
    //TODO: Implement - this is currently still the Rect implementation
    return point.x >= this.x
      && point.x <= this.x + this.r
      && point.y >= this.y
      && point.y <= this.y + this.r;
  }

  /**
   * @param {number} x left
   * @param {number} y top
   * @param {number} r radius
   */
  setAll(x, y, r) {
    this.x = x;
    this.y = y;
    this.w = r * 2;
    this.h = r * 2;
    this.r = r;
  }

  /**
   * @param {CanvasRenderingContext2D} ctx
   */
  fill(ctx) {
    const root3over2   = Math.sqrt(3) / 2;
    var { x, y, r } = this;
    ctx.moveTo(x, y - r);
    ctx.lineTo(x + root3over2 * r, y - 1 / 2 * r);
    ctx.lineTo(x + root3over2 * r, y + 1 / 2 * r);
    ctx.lineTo(x, y + r);
    ctx.lineTo(x - root3over2 * r, y + 1 / 2 * r);
    ctx.lineTo(x - root3over2 * r, y - 1 / 2 * r);
    ctx.lineTo(x, y - r);
    ctx.fill();
  }

  /**
   * Makes instances of Rect iterable. Enables use of the spread operator, for-of loops, etc.
   * @example
   * const rect = new Rect(2, 4, 5, 7);
   * logRect = (a, b, c, d) => console.log(`${a} ${b} ${c} ${d}`);
   * logPoint(rect.x, rect.y, rect.w, rect.h); // 2 4 5 7
   * logRect(...rect); // 2 4 5 7
   */
  *[Symbol.iterator]() {
    yield this.x;
    yield this.y;
    yield this.w;
    yield this.h;
    yield this.r;
  }

}
