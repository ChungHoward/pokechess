import HeldItem from '../class/items/HeldItem.js';
import KeyItem from '../class/items/KeyItem.js';
import Pokeball from '../class/items/Pokeball.js';
import Pokemon from '../class/pokemon/Pokemon.js';
import TM from '../class/items/TM.js';
import { Type } from '../class/enums/Type.js';
import { Earthquake, IceBeam, Thunder } from './Skill.lib.js';

// ----- Not Items ----- Utility functions ----- //
/** @param {Pokemon} pkmn */
const indexOfNegativeStatus = (pkmn) => pkmn.statusCondition.findIndex(status => !status.isBuff && status.duration >= 8);
/** @param {Pokemon} pkmn */
const isCurrentHPBelowHalf = (pkmn) => Math.ceil(pkmn.current.HP / pkmn.base.HP) <= Math.floor(pkmn.base.HP / 2);

/* #region ----- Held Items ----- */
const AmuletCoin = new HeldItem('Amulet Coin', 1, 2,
  'For 1 battle, earn 1 currency after using a Skill',
  () => true, (pkmn) => {
    pkmn.skill.addEffect((self, targets) => self.trainer.currency++);
    pkmn.heldItem = null;
  });
const BlackBelt = new HeldItem('Black Belt', 1, 3,
  `For 1 battle, ${Type.FIGHT} Pokemon get +20% ATK and SATK`,
  () => true, (pkmn) => {
    if (pkmn.types.includes(Type.FIGHT)) {
      pkmn.base.ATK *= 1.2;
      pkmn.base.SATK *= 1.2;
      pkmn.heldItem = null;
    }
  });
const BlackGlasses = new HeldItem('Black Glasses', 1, 3,
  `For 1 battle, ${Type.DARK} Pokemon get +20% ATK and SATK`,
  () => true, (pkmn) => {
    if (pkmn.types.includes(Type.DARK)) {
      pkmn.base.ATK *= 1.2;
      pkmn.base.SATK *= 1.2;
      pkmn.heldItem = null;
    }
  });
const Charcoal = new HeldItem('Charcoal', 1, 3,
  `For 1 battle, ${Type.FIRE} Pokemon get +20% ATK and SATK`,
  () => true, (pkmn) => {
    if (pkmn.types.includes(Type.FIRE)) {
      pkmn.base.ATK *= 1.2;
      pkmn.base.SATK *= 1.2;
      pkmn.heldItem = null;
    }
  });
const DragonFang = new HeldItem('Dragon Fang', 1, 3,
  `For 1 battle, ${Type.DRAGN} Pokemon get +20% ATK and SATK`,
  () => true, (pkmn) => {
    if (pkmn.types.includes(Type.DRAGN)) {
      pkmn.base.ATK *= 1.2;
      pkmn.base.SATK *= 1.2;
      pkmn.heldItem = null;
    }
  });
const HardStone = new HeldItem('Hard Stone', 1, 3,
  `For 1 battle, ${Type.ROCK} Pokemon get +20% ATK and SATK`,
  () => true, (pkmn) => {
    if (pkmn.types.includes(Type.ROCK)) {
      pkmn.base.ATK *= 1.2;
      pkmn.base.SATK *= 1.2;
      pkmn.heldItem = null;
    }
  });
const LeppaBerry = new HeldItem('Leppa Berry', 1, 1,
  'Consumed to gain +10 PP when HP falls below half',
  (pkmn) => isCurrentHPBelowHalf(pkmn), (pkmn) => {
    pkmn.pp += 10;
    pkmn.heldItem = null;
  });
const LuckyEgg = new HeldItem('Lucky Egg', 2, 2,
  'For 1 battle, level up when KOing a Pokemon with a Skill',
  () => true, (pkmn) => {
    pkmn.skill.addEffect((self, targets) => {
      self.level += targets.reduce((numDefeated, target) => {
        return numDefeated + (target.current.HP <= 0 ? 1 : 0);
      });
    });
    pkmn.heldItem = null;
  });
const LumBerry = new HeldItem('Lum Berry', 1, 1,
  'Consumed to cure negative status ailments',
  (pkmn) => indexOfNegativeStatus(pkmn) >= 0, (pkmn) => {
    pkmn.statusCondition.splice(indexOfNegativeStatus(pkmn), 1);
    pkmn.heldItem = null;
  });
const Magnet = new HeldItem('Magnet', 1, 3,
  `For 1 battle, ${Type.ELEC} Pokemon get +20% ATK and SATK`,
  () => true, (pkmn) => {
    if (pkmn.types.includes(Type.ELEC)) {
      pkmn.base.ATK *= 1.2;
      pkmn.base.SATK *= 1.2;
      pkmn.heldItem = null;
    }
  });
const MetalCoat = new HeldItem('Metal Coat', 1, 3,
  `For 1 battle, ${Type.STEEL} Pokemon get +20% ATK and SATK`,
  () => true, (pkmn) => {
    if (pkmn.types.includes(Type.STEEL)) {
      pkmn.base.ATK *= 1.2;
      pkmn.base.SATK *= 1.2;
      pkmn.heldItem = null;
    }
  });
const MiracleSeed = new HeldItem('Miracle Seed', 1, 3,
  `For 1 battle, ${Type.GRASS} Pokemon get +20% ATK and SATK`,
  () => true, (pkmn) => {
    if (pkmn.types.includes(Type.GRASS)) {
      pkmn.base.ATK *= 1.2;
      pkmn.base.SATK *= 1.2;
      pkmn.heldItem = null;
    }
  });
const MysticWater = new HeldItem('Mystic Water', 1, 3,
  `For 1 battle, ${Type.WATER} Pokemon get +20% ATK and SATK`,
  () => true, (pkmn) => {
    if (pkmn.types.includes(Type.WATER)) {
      pkmn.base.ATK *= 1.2;
      pkmn.base.SATK *= 1.2;
      pkmn.heldItem = null;
    }
  });
const NeverMeltIce = new HeldItem('Never-Melt Ice', 1, 3,
  `For 1 battle, ${Type.ICE} Pokemon get +20% ATK and SATK`,
  () => true, (pkmn) => {
    if (pkmn.types.includes(Type.ICE)) {
      pkmn.base.ATK *= 1.2;
      pkmn.base.SATK *= 1.2;
      pkmn.heldItem = null;
    }
  });
const PixiePlate = new HeldItem('Pixie Plate', 1, 3,
  `For 1 battle, ${Type.FAIRY} Pokemon get +20% ATK and SATK`,
  () => true, (pkmn) => {
    if (pkmn.types.includes(Type.FAIRY)) {
      pkmn.base.ATK *= 1.2;
      pkmn.base.SATK *= 1.2;
      pkmn.heldItem = null;
    }
  });
const PoisonBarb = new HeldItem('Poison Barb', 1, 3,
  `For 1 battle, ${Type.POISN} Pokemon get +20% ATK and SATK`,
  () => true, (pkmn) => {
    if (pkmn.types.includes(Type.POISN)) {
      pkmn.base.ATK *= 1.2;
      pkmn.base.SATK *= 1.2;
      pkmn.heldItem = null;
    }
  });
const SharpBeak = new HeldItem('Sharp Beak', 1, 3,
  `For 1 battle, ${Type.FLYNG} Pokemon get +20% ATK and SATK`,
  () => true, (pkmn) => {
    if (pkmn.types.includes(Type.FLYNG)) {
      pkmn.base.ATK *= 1.2;
      pkmn.base.SATK *= 1.2;
      pkmn.heldItem = null;
    }
  });
const ShellBell = new HeldItem('Shell Bell', 1, 3,
  'For 1 battle, heals 1/8 of HP after using a Skill',
  (pkmn) => true, (pkmn) => {
    pkmn.skill.addEffect((self, targets) => self.current.HP += self.base.HP / 8);
    pkmn.heldItem = null;
  });
const SilverPowder = new HeldItem('Silver Powder', 1, 3,
  `For 1 battle, ${Type.BUG} Pokemon get +20% ATK and SATK`,
  () => true, (pkmn) => {
    if (pkmn.types.includes(Type.BUG)) {
      pkmn.base.ATK *= 1.2;
      pkmn.base.SATK *= 1.2;
      pkmn.heldItem = null;
    }
  });
const SitrusBerry = new HeldItem('Sitrus Berry', 1, 1,
  'Consumed to heal 25% HP when HP falls below half',
  (pkmn) => isCurrentHPBelowHalf(pkmn), (pkmn) => {
    pkmn.current.HP += Math.ceil(pkmn.base.HP / 4);
    pkmn.heldItem = null;
  });
const SilkScarf = new HeldItem('Silk Scarf', 1, 3,
  `For 1 battle, ${Type.NORML} Pokemon get +20% ATK and SATK`,
  () => true, (pkmn) => {
    if (pkmn.types.includes(Type.NORML)) {
      pkmn.base.ATK *= 1.2;
      pkmn.base.SATK *= 1.2;
      pkmn.heldItem = null;
    }
  });
const SoftSand = new HeldItem('Soft Sand', 1, 3,
  `For 1 battle, ${Type.GRND} Pokemon get +20% ATK and SATK`,
  () => true, (pkmn) => {
    if (pkmn.types.includes(Type.GRND)) {
      pkmn.base.ATK *= 1.2;
      pkmn.base.SATK *= 1.2;
      pkmn.heldItem = null;
    }
  });
const SpellTag = new HeldItem('Spell Tag', 1, 3,
  `For 1 battle, ${Type.GHOST} Pokemon get +20% ATK and SATK`,
  () => true, (pkmn) => {
    if (pkmn.types.includes(Type.GHOST)) {
      pkmn.base.ATK *= 1.2;
      pkmn.base.SATK *= 1.2;
      pkmn.heldItem = null;
    }
  });
const TwistedSpoon = new HeldItem('Twisted Spoon', 1, 3,
  `For 1 battle, ${Type.PSYCH} Pokemon get +20% ATK and SATK`,
  () => true, (pkmn) => {
    if (pkmn.types.includes(Type.PSYCH)) {
      pkmn.base.ATK *= 1.2;
      pkmn.base.SATK *= 1.2;
      pkmn.heldItem = null;
    }
  });
/* #endregion */

//----- Key Items -----//


/* #region ----- Pokeballs ----- */
export const PokeBall = new Pokeball('Poke Ball', 1, 1,
  'Can capture ⭐ Pokemon',
  (pkmn) => pkmn.rarity === 1);
const GreatBall = new Pokeball('Great Ball', 2, 1,
  'Can capture ⭐⭐ or lower Pokemon',
  (pkmn) => pkmn.rarity === 2);
const UltraBall = new Pokeball('Ultra Ball', 3, 3,
  'Can capture ⭐⭐⭐ or lower Pokemon',
  (pkmn) => [2, 3].includes(pkmn.rarity));
const MasterBall = new Pokeball('Master Ball', 5, 5,
  'Can capture any Pokemon',
  () => true);
const FastBall = new Pokeball('Fast Ball', 2, 2,
  'Can capture Pokemon with at least 80 SPE',
  (pkmn) => pkmn.base.SPE >= 80);
const DuskBall = new Pokeball('Dusk Ball', 3, 3,
  'Can capture any dark or ghost type Pokemon',
  (pkmn) => pkmn.types.includes(Type.DARK) || pkmn.types.includes(Type.GHOST));
const HeavyBall = new Pokeball('Heavy Ball', 3, 3,
  'Can capture any rock or steel type Pokemon',
  (pkmn) => pkmn.types.includes(Type.ROCK) || pkmn.types.includes(Type.STEEL));
const NetBall = new Pokeball('Net Ball', 3, 3,
  'Can capture any bug or water type Pokemon',
  (pkmn) => pkmn.types.includes(Type.BUG) || pkmn.types.includes(Type.WATER));
const RepeatBall = new Pokeball('Repeat Ball', 1, 2,
  'Can capture Pokemon species on your bench');
const FriendBall = new Pokeball('Friend Ball', 2, 3,
  'Can capture Pokemon types on your bench');
/* #endregion */

/* #region ----- TMs ----- */
const IceBeamTM = new TM('TM 13 Ice Beam', 2, 3,
  'Fires a piercing ice beam that freezes enemies for 8 seconds',
  IceBeam);
const ThunderTM = new TM('TM 25 Thunder', 2, 3,
  'Showers a huge area in lightning that paralyzes enemies for 8 seconds',
  Thunder);
const EarthquakeTM = new TM('TM 26 Earthquake', 2, 3,
  'Devestates all non-flying Pokemon in a large radius with a deadly quake.',
  Earthquake);
/* #endregion */

export const AllItems = [
  // Held Items
  AmuletCoin,
  BlackBelt,
  BlackGlasses,
  Charcoal,
  DragonFang,
  HardStone,
  LeppaBerry,
  LuckyEgg,
  LumBerry,
  Magnet,
  MetalCoat,
  MiracleSeed,
  MysticWater,
  NeverMeltIce,
  PixiePlate,
  PoisonBarb,
  SharpBeak,
  ShellBell,
  SilverPowder,
  SitrusBerry,
  SilkScarf,
  SoftSand,
  SpellTag,
  TwistedSpoon,
  // Pokeballs
  GreatBall,
  UltraBall,
  MasterBall,
  FastBall,
  DuskBall,
  HeavyBall,
  NetBall,
  RepeatBall,
  FriendBall,
  // TMs
  IceBeamTM,
  ThunderTM,
  EarthquakeTM
];
