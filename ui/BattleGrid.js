import ICanDraw from './ICanDraw.js';

export default class BattleGrid extends ICanDraw {

  constructor(dim) {
    super(dim);
  }

  /**
   * @param {CanvasRenderingContext2D} ctx 
   * @param {GameState} gameState 
   */
  draw(ctx, gameState) {
    for (let i = 0; i < 7; i++) {
      for (let j = 0; j < 6 + i % 2; j++) {
        const centerX = 50 * (j + 1) - 25 * (i % 2) + this.dim.left;
        const centerY = 50 * (i + 1) + this.dim.top;
        ctx.beginPath();
        ctx.moveTo(centerX, centerY - 33);
        ctx.lineTo(centerX + 25, centerY - 17);
        ctx.lineTo(centerX + 25, centerY + 17);
        ctx.lineTo(centerX, centerY + 33);
        ctx.lineTo(centerX - 25, centerY + 17);
        ctx.lineTo(centerX - 25, centerY - 17);
        ctx.lineTo(centerX, centerY - 33);
        ctx.stroke();
        ctx.fillStyle = '#ffffff';
        if (gameState.field[i][j] && gameState.field[i][j].id) {
          ctx.fillStyle = gameState.field[i][j].trainer.color;
        }
        ctx.fill();
      }
    }

    for (let i = 0; i < 7; i++) {
      for (let j = 0; j < 6 + i % 2; j++) {
        const centerX = 50 * (j + 1) - 25 * (i % 2) + this.dim.left;
        const centerY = 50 * (i + 1) + this.dim.top;
        if (gameState.field[i][j]
          && gameState.field[i][j].id) {
          const pokemon = gameState.field[i][j];
          const img = this.getPokemonImg(gameState, pokemon.id);
          ctx.drawImage(img, centerX - 25, centerY - 25, 50, 50);
          ctx.fillStyle = '#00ff00';
          const hpPercent = pokemon.current.HP / pokemon.base.HP;
          ctx.fillRect(centerX - 25, centerY - 25, 40 * hpPercent, 5);
          ctx.fillStyle = '#ff0000';
          ctx.fillRect(centerX - 25 + 40 * hpPercent, centerY - 25, 40 - 40 * hpPercent, 5);
          if (pokemon.skill) {
            const ppPercent = pokemon.pp / pokemon.skill.ppCost;
            ctx.fillStyle = '#0000ff';
            ctx.fillRect(centerX - 25, centerY - 20, 40 * ppPercent, 5);
            ctx.fillStyle = '#ffffff';
            ctx.fillRect(centerX - 25 + 40 * ppPercent, centerY - 20, 40 - 40 * ppPercent, 5);
          }
        }
      }
    }
  }

  /**
   * @param {MouseEvent} evt 
   * @param {boolean} isRight Is right click
   * @param {GameState} gameState 
   */
  mouseClick(evt, isRight, gameState) {
    const x = evt.clientX;
    const y = evt.clientY + document.scrollingElement.scrollTop;
    if (y > this.dim.height) {
      return;
    }
    const row = Math.floor((y - 25) / 50);
    const col = Math.floor((x - 25 * ((row + 1) % 2)) / 50);
    if (gameState.field[row] && !gameState.field[row][col] && gameState.heldPokemon) {
      gameState.serverConnection.send(JSON.stringify({
        label: 'PUT_POKEMON',
        data: {
          row, col, id:gameState.heldPokemon.id, trainer:gameState.heldPokemon.trainer.name
        }
      }));
      //gameState.field[row][col] = gameState.heldPokemon;
      gameState.heldPokemon = undefined;
    }
  }

}
