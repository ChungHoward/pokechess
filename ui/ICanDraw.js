import Pokemon from '../scripts/class/pokemon/Pokemon.js';

export default class ICanDraw {

  /**
   * @typedef Dim
   *  @property {number} top
   *  @property {number} left
   *  @property {number} width
   *  @property {number} height
   * @param {Dim} dim 
   */
  constructor(dim) {
    this.dim = {...dim};
  }

  /**
   * @typedef Img
   *  @property {Pokemon[]}             pokemon   An array of Pokemon
   * @typedef Caches
   *  @property {Img}                   img       Wrapper for an array of Pokemon
   *  @property {Pokemon[]}             pokeData  An array of Pokemon again
   * @typedef GameState
   *   @property {Pokemon[][]}          field     The game board, a 2-d array of empty tiles and sometimes Pokemon
   *  @property {Caches}                caches    Holds an Array<Pokemon> and a wrapper for another Array<Pokemon>
   * @param {CanvasRenderingContext2D}  ctx       Used for drawing stuff
   * @param {GameState}                 gameState Holds cached data during a game
   */
  draw(ctx, gameState) {}

  /**
   * @param {Event} evt 
   * @param {boolean} isRight Is right click
   */
  mouseClick(evt, isRight) {}

  /**
   * @param {GameState} gameState 
   * @param {number} id 
   */
  addPokemonToImgCache(gameState, id) {
    var img = new Image;
    img.src = `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${id}.png`;
    gameState.caches.img.pokemon[id] = img;
  }

  /**
   * @param {GameState} gameState 
   * @param {number} id 
   */
  getPokemonImg(gameState, id) {
    if (!gameState.caches.img.pokemon[id]) {
      this.addPokemonToImgCache(gameState, id);
    }
    return gameState.caches.img.pokemon[id];
  }

}
