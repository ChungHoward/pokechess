### Ideas

If we give Pokemon a second moveslot.
We would need to change PP so that it always accumulates until it uses the Skill with the largest PP cost. When the current PP accumulates enough to use a skill, it does not drain the current PP until you use your most expensive skill. Ex: If your Pokemon knows Bubble (3pp) and Surf (10pp), you will use Bubble on turn 3, then Surf on turn 10, then Bubble on turn 13, Surf on 20, etc.
Alternatively it could use separate PP counters. So it would Bubble on turn 3, 6, 9, 12, 15, 18, 21. And Surf on 10, 20... etc.