
export default class HexGridUtil {

  constructor() {}

  /**
   * @param {number} x 
   * @param {number} y 
   */
  getAllNeighborPairs(x,y) {
    return (y%2==0?[
      {x:x+1,y:y-1},
      {x:x,y:y-1},
      {x:x+1,y:y},
      {x:x,y:y+1},
      {x:x+1,y:y+1},
      {x:x-1,y:y},
    ]:[
      {x:x-1,y:y-1},
      {x:x,y:y-1},
      {x:x+1,y:y},
      {x:x,y:y+1},
      {x:x-1,y:y+1},
      {x:x-1,y:y},
    ]).filter(p => p.x>=0 && p.y>=0 && p.x<=5+y%2 && p.y<=6);
  }

}
