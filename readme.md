### Pokechess

Pokemon-themed auto chess

**Features**

* Pokemon
  * [x] Pokemon have base stats and current stats
  * [x] Pokemon have Skills
  * [x] Pokemon can hold Items
  * [x] Pokemon can have status conditions
  * [x] Pokemon can fight other Pokemon
  * [x] Pokemon can evolve
* Skills
  * [x] Skills can have effects
* Board
  * [x] The play area consisting of hexagonal tiles
  * [x] Pokemon can move from tile to tile
  * [x] Each tile can only occupy one Pokemon
  * [ ] Players can place Pokemon on half of the tiles
  * [ ] Pokemon on the board is private information during the Main phase and Public during the Battle phase
* Bench
  * [ ] Each player has their own Bench
  * [ ] Benches show the Pokemon on your team as well as captured Pokemon
  * [x] Can rearrange Pokemon on your Bench
* Team
  * [ ] Pokemon on your Bench can be placed on the Board add it to your team
  * [ ] Pokemon on the Board can be moved back to your Bench to remove it from the team
  * [ ] A team may consist of up to 6 Pokemon
  * [ ] Each player's Team is public information
* Wild
  * [x] Pokemon in the Wild can be captured using Pokeballs
  * [x] Capturing a Pokemon adds them to your Bench and removes them from the Wild
  * [x] Wild can be refreshed
  * [ ] Each player has their own Wild
* Deck
  * [ ] Pokemon in the Deck determines the possible Pokemon you can see in the Wild
  * [ ] Items in the Deck determines the possible Items you can see in the Shop
  * [ ] Pokemon and Items in the Deck are hidden information to all players, but its contents are deterministic
  * [ ] Pokemon and Items in the Deck are shared between players
* Shop
  * [x] Items in the Shop can be purchased with currency
  * [x] Purchased items are placed in your Inventory and removed from the Shop
  * [x] Shop can be refreshed
* Inventory
  * [x] Each player has their own Inventory
  * [x] Contains items purchased from the Shop
  * [ ] Your Inventory stacks duplicate items
* Trainer
  * [ ] Players (AKA Trainers) can earn badges by defeating other Trainers
  * [x] Trainers have exp and can level up
  * [ ] Their level determines the rarity of Pokemon and Items they can encounter in the Wild and Shop
  * [x] Trainers can buy exp
* Turn
  * [ ] A turn consists of 2 phases: Main and Battle
  * [ ] Each player takes their turns simultaneously
* Main
  * [ ] The Main phase lasts for 30 seconds
  * [ ] At the beginning of the Main phase, both players:
    * [ ] Earn currency plus interest
    * [ ] Shop Items and Wild Pokemon refresh using the Deck
    * [ ] Pokemon in each player's Bench and team heal some HP
  * [ ] During this phase, players can:
    * [ ] Capture Pokemon
    * [ ] Move Pokemon from the Bench to the Board to form a Team
    * [ ] Remove Pokemon on the Board to add them to the Bench
    * [x] Buy Items from the Shop
    * [x] Buy exp
    * [x] Refresh the Shop
    * [ ] Use Items from the Inventory
* Battle
  * [x] A Battle is the phase where one player's Team auto-battles the other player's Team
  * [ ] A player loses the battle when all Pokemon on their team faints
  * [ ] A player wins when the opposing player loses, ties results in both players losing
  * [ ] When a player wins, the winner earns a Badge representing the opponent he or she defeated
* Game
  * [ ] A Game consists of round robin Battles
  * [ ] The first player to win a Badge from each opponent in the game wins

**Items**

_Pokeballs_

Pokeballs are used to capture Pokemon.

* [x] Poke ball - Can capture weak Pokemon
* [x] Great ball - Can capture average Pokemon
* [x] Ultra ball - Can capture powerful Pokemon
* [x] Master ball - Can capture all Pokemon
* [x] Fast ball - Can capture Pokemon with >= 80 SPE
* [x] Net ball - Can capture any bug or water type Pokemon
* [x] Dusk ball - Can capture any dark or ghost type Pokemon
* [x] Repeat ball - Can capture previously caught Pokemon species
* [x] Friend ball - Can capture previously caught Pokemon types
* etc

_Held Item_

Pokemon can hold one held item to bring into battle. These items can be consumed during battle to provide a helpful effect.

* [x] Amulet Coin - Earn 1 currency after using a Skill for the next battle
* [x] Charcoal - Raises the ATK and SATK of fire Pokemon by 20% for the next battle
* [x] Leppa Berry - Consumed to gain +10 PP, when HP falls below half
* [x] Lucky Egg - Pokemon gains 1 level if it defeats a Pokemon using a skill
* [x] Lum Berry - Consumed to cure negative status ailments when one is inflicted
* [x] Shell Bell - Heals 1/8 of HP after using a Skill for the next battle
* [x] Sitrus Berry - Consumed to heal 25% HP, when HP falls below half
* etc

_Technical Machines (TMs)_

TMs can be consumed during your Main phase to replace a Pokemon's Skill if they match the Skill's type.

* [ ] TM 06 Toxic - Inflicts deadly poison to a single target, the damage grows over time. Poison, 0 power, 10 PP
* [ ] TM 13 Ice Beam - Fires a piercing ice-cold beam that slows the victims for 5 seconds. Ice, 90 power, 15 PP
* [ ] TM 26 Earthquake - Devestates all non-flying type Pokemon in a large radius with a deadly quake. Ground, 100 power, 20 PP
* [ ] TM 97 Dark Pulse - Emits a nasty aura in a short radius. Enemy targets in range skip their next basic attack. Dark, 80 power, 13 PP
* etc

_Key Items_

Key Items provide passive effects that last for the entire game

* [ ] Town Map - Increases the encounter rate of strong Pokemon in the Wild
* [ ] Berry Planter - Allows you to plant berries during each Main Phase, providing a steady supply of berries each turn
* [ ] Old Rod - Allows you to fish for weak Wild Pokemon each Main Phase for $1, adding a new weak Pokemon to the Wild
* [ ] Bicycle - Allows you to refresh all Wild Pokemon at the cost of 45 seconds of this turn's time limit
* [ ] Pokedex - Increases the encounter rate of previously captured Pokemon species
* [ ] VS Seeker - Adds a powerful Pokemon to the deck upon purchase and at the beginning of each Main phase
* [ ] Sea Incense - Increases the encounter rate of Water Pokemon
* [ ] Rose Incense - Increases the encounter rate of Grass Pokemon
* [ ] Rock Incense - Increases the encounter rate of Rock Pokemon
* [ ] Odd Incense - Increases the encounter rate of Psychic Pokemon
* [ ] Full Incense - Increases the encounter rate of Bug Pokemon
* [ ] Lax Incense - Increases the encounter rate of Ground Pokemon
* [ ] Wave Incense - Increases the encounter rate of Ice Pokemon
* [ ] Pure Incense - Increases the encounter rate of Normal Pokemon
* [ ] Luck Incense - Increases the encounter rate of rarity=5 Pokemon
* etc