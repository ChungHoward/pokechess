
import HexGridUtil from '../util/HexGridUtil.js';
import Trainer from '../scripts/class/trainer/Trainer.js'

export default class GameSim {

  constructor() {}

  /**
   * Find the next unit that is supposed to act, then move or attack with it
   */
  step(gameState) {
    var { getAllNeighborPairs } = new HexGridUtil();

    var nextUnitCoords;
    var nextUnitInitiative = 999;
    for (var i=0;i<7;i++) {
      for (var j=0;j<7;j++) {
        if (gameState.field[i][j] && gameState.field[i][j].id) {
          var unit = gameState.field[i][j];
          if (!unit.baseInitiative) {
            unit.baseInitiative = (400-unit.base.SPE)/400;
            unit.currentInitiative = unit.baseInitiative;
          }
          if (unit.currentInitiative < nextUnitInitiative) {
            nextUnitCoords = {x:j,y:i};
            nextUnitInitiative = unit.currentInitiative;
          }
        }
      }
    }
    if (!nextUnitCoords) {
      return;
    }

    var nextUnit = gameState.field[nextUnitCoords.y][nextUnitCoords.x];
    // Apply initiative reduction
    
    for (var i=0;i<7;i++) {
      for (var j=0;j<7;j++) {
        if (gameState.field[i][j] && gameState.field[i][j].id) {
          var unit = gameState.field[i][j];
          if (i==nextUnitCoords.y && j==nextUnitCoords.x) {
            unit.currentInitiative = unit.baseInitiative;
          } else {
            unit.currentInitiative -= nextUnitInitiative;
          }
        }
      }
    }


    // Apply AI to Target/Move/Attack
    // First label all the spaces that you can attack from
    // If you are in one of those spaces, perform attack
    // If not, do djikstra to get shortest path to one of those spaces
    // Move that way

    // Number 1 will be used to represent places from which the unit can attack i.e. target spaces
    // Number 2 will be used to represent paths traversed
    var pathFindingCache = [];
    for (var i=0;i<7;i++) {
      pathFindingCache[i]=[];
    }

    // Labelling all spaces that you can attack from
    for (var i=0;i<7;i++) {
      for (var j=0;j<7;j++) {
        if (gameState.field[i][j] && gameState.field[i][j].id) {
          var unit = gameState.field[i][j];
          if (unit.trainer.name != nextUnit.trainer.name) {
            var attackSpots = getAllNeighborPairs(j,i);
            attackSpots.forEach(p => {
              pathFindingCache[p.y][p.x] = 1;
            });
          }
        }
      }
    }

    if (pathFindingCache[nextUnitCoords.y][nextUnitCoords.x]) {
      var attackRange = getAllNeighborPairs(nextUnitCoords.x,nextUnitCoords.y);
      var attackTarget = attackRange.filter(p => {
        return gameState.field[p.y][p.x] && gameState.field[p.y][p.x].id && gameState.field[p.y][p.x].trainer.name != nextUnit.trainer.name
      })[0];
      gameState.field[nextUnitCoords.y][nextUnitCoords.x].fight(gameState.field[attackTarget.y][attackTarget.x]);
      if (gameState.field[attackTarget.y][attackTarget.x].current.HP<=0) {
        gameState.field[attackTarget.y][attackTarget.x] = 0;
      }
    } else {
      // Do pathfinding algorithm to see where to move next
      var pathingFloodQueue = getAllNeighborPairs(nextUnitCoords.x, nextUnitCoords.y).filter(p => !gameState.field[p.y][p.x]).map(p => [p,p]);
      pathFindingCache[nextUnitCoords.y][nextUnitCoords.x]=2;
      if (pathingFloodQueue.filter(p => pathFindingCache[p[0].y][p[0].x]==1).length>=1) {
        targetMoveCoord = pathingFloodQueue.filter(p => pathFindingCache[p[0].y][p[0].x]==1)[0][0];
      }
      pathingFloodQueue.forEach(p => {
        pathFindingCache[p[0].y][p[0].x]=2;
      });
      var targetMoveCoord;
      while (pathingFloodQueue.length>0 && !targetMoveCoord) {
        var newestPath = pathingFloodQueue.splice(0,1)[0];
        var newestCoord = newestPath[1];
        if (pathFindingCache[newestCoord.y][newestCoord.x]==1) {
          targetMoveCoord = newestPath[0];
        } else {
          var traversableNeighbors = getAllNeighborPairs(newestCoord.x, newestCoord.y).filter(p => !gameState.field[p.y][p.x]).filter(p => pathFindingCache[p.y][p.x]!=2);
          if (traversableNeighbors.filter(p => pathFindingCache[p.y][p.x]==1).length>=1) {
            // Path found to somewhere attackable
            targetMoveCoord = newestPath[0];
          } else {
            // Path not found yet, put all new neighbors into the queue
            traversableNeighbors.forEach(p => {
              pathingFloodQueue.push([newestPath[0], {x:p.x,y:p.y}]);
              pathFindingCache[p.y][p.x] = 2;
            });
            pathFindingCache[newestPath[0].y][newestPath[0].x] = 2;
          }
        }
      }
      if (targetMoveCoord) {
        // Path found, move unit from current location to target
        var unit = gameState.field[nextUnitCoords.y][nextUnitCoords.x];
        gameState.field[nextUnitCoords.y][nextUnitCoords.x] = 0;
        gameState.field[targetMoveCoord.y][targetMoveCoord.x] = unit;
      }
      
    }
  }

  // Capture all data that needs to be sent over the network
  // This is just the field for now
  serialize(gameState) {
    return JSON.stringify(gameState.field.map(r => {
      return r.map(c => {
        if (!c) {
          return 0;
        } else {
          var {id} = c;
          var current = {...c.current};
          var trainerName = c.trainer.name;
          console.log(c.trainer);
          console.log(trainerName);
          var skillName = c.skill && c.skill.name;
          return {id, current, trainerName, skillName};
        }
      });
    }));
  }

  // Reconstruct the necessary parts of gameState using info from the network
  deserialize(gameState, s) {
    var newField = JSON.parse(s);
    gameState.field = newField.map(r => {
      return r.map(c => {
        if (!c) {
          return 0;
        } else {
          var {id, current, trainerName, skillName} = c;
          var pokemon = gameState.caches.pokeData[id].cloneForTrainer(new Trainer(trainerName));
          pokemon.current = current;
          return pokemon;
        }
      });
    });
  }

}
